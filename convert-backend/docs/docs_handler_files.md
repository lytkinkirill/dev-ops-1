# Files handler

This section describes methods for uploading, receiving, and sending files to the server.

## getFilesData

The ***getFilesData*** method unpacks a request containing the username and a list of files.

```go
func (h *Handler) getFilesData(c *gin.Context) {
	// Unpacking the JSON request
	var requestData map[string]interface{}
	if err := c.ShouldBindJSON(&requestData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Ошибка при разборе JSON"})
		return
	}

	// Getting username from request
	_, ok := requestData["username"].(string)
	if !ok {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Некорректное имя пользователя"})
		return
	}
	var userFiles, err = h.r.FileRepo.GetFiles(requestData["username"].(string))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Ошибка при чтении списка файлов"})
		return
	}

	// Sending data to the client in JSON format
	c.JSON(http.StatusOK, userFiles)
}
```

## uploadFile

The ***uploadFile*** method receives a file request, processes the file name and its extension, creates a new file with the current time in the file name, sends it to the server and saves it in the database.

```go
func (h *Handler) uploadFile(c *gin.Context) {
	// Receiving a file from a request
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(400, gin.H{"error": "Ошибка при получении файла"})
		return
	}
	defer file.Close()

	var filename string

	// Separate the file name and extension
	strings.TrimSpace(header.Filename)
	parts := strings.Split(header.Filename, ".")
	if len(parts) >= 2 {
		filenameWithoutExtension := strings.Join(parts[:len(parts)-1], ".")
		fileExtension := parts[len(parts)-1]

		// Get the current time in Unix timestamp format
		currentTime := time.Now().Unix()

		// Generate a new file name with a timestamp before the extension
		filename = fmt.Sprintf("%s_%d.%s", filenameWithoutExtension, currentTime, fileExtension)
	} else {
		fmt.Println("File name isn't processed")
	}

	// Define the path to save the file with a timestamp in the name
	// filename := fmt.Sprintf("%s_%d", header.Filename, time.Now().Unix())
	filepath := "../go/storage/" + filename

	// Create a file on the server to save
	uploadedFile, err := os.Create(filepath)
	if err != nil {
		fmt.Printf("Ошибка при создании файла: %s\n", err)
		c.JSON(500, gin.H{"error": "Ошибка при создании файла на сервере"})
		return
	}
	defer uploadedFile.Close()

	// Copying the contents of the file from the request to a file on the server
	_, err = io.Copy(uploadedFile, file)
	if err != nil {
		c.JSON(500, gin.H{"error": "Ошибка при копировании файла"})
		return
	}

	c.JSON(200, gin.H{"message": "Файл успешно загружен и сохранен на сервере"})

	// Get username
	username := c.PostForm("username")

	_ = time.Now()

	// File upload request handler to database
	err = h.r.FileRepo.UploadFile(username, filename, time.Now())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Ошибка при записи пути в базу данных"})
		return
	}

	// Returning a successful response
	c.JSON(http.StatusOK, gin.H{"message": "Файл успешно загружен и записан в базу данных", "success": "true"})
}
```

## getFile

The ***getFile*** method processes the request, finds the file in the database and sends it to the user.

```go
func (h *Handler) getFile(c *gin.Context) {
	// Unpacking the JSON request
	/*var requestData map[string]interface{}
	if err := c.ShouldBindJSON(&requestData); err != nil {
		fmt.Printf("Ошибка при разборе JSON: %s\n", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": "Ошибка при разборе JSON"})
		return
	}*/
	fileId := c.Request.URL.Query().Get("id")

	// Getting filename from request
	/*file, ok := requestData["filename"].(string)
	if !ok {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Некорректное имя пользователя"})
		return
	}*/

	// Searching for a file in the database
	filename, err := h.r.FileRepo.GetFile(fileId)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Файл не найден"})
		return
	}

	// Determining the path to the file in the storage
	filePath := fmt.Sprintf("../go/storage/%s", filename)

	// Sending the file to the client
	c.FileAttachment(filePath, filename)
}
```