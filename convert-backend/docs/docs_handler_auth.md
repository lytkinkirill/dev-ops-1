# Authentication handler

Authentication is the process that helps identify who is the users. Also authorization is the process of determining what a user can do.

## Login

```go
func (h *Handler) login(c *gin.Context) {
    // User model
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Checking user registration in the database
	authorizedUser, err := h.r.UserRepo.GetUserByEmail(user.Email)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}

	username := authorizedUser.Username

    // Returning a successful response
	c.JSON(http.StatusOK, gin.H{"message": "Авторизация прошла успешно", "success": "true", "username": username})
}
```

## Logout

```go
func (h *Handler) logout(c *gin.Context) {
    // Returning a successful response
	c.JSON(http.StatusOK, gin.H{"message": "Выход выполнен", "success": "true"})
}
```

## Register

```go
func (h *Handler) register(c *gin.Context) {
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check if user already exists
	if _, err := h.r.UserRepo.GetUserByEmail(user.Email); err == nil {
		c.JSON(http.StatusConflict, gin.H{"message": "Такой пользователь уже существует!", "success": false})
		return
	}

	// The user does not exist, create a new user
	if err := h.r.UserRepo.CreateUser(user.Username, user.Password, user.Email); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Returning a successful response
	c.JSON(http.StatusCreated, gin.H{"message": "Пользователь успешно создан", "success": "true"})
}

```