# Table of contents

- [Home](README.md)

## Client Usage

Add Frontend docs

## Server Usage

- [Authentication handler](docs_handler_auth.md)
- [File handler](docs_handler_files.md)
- [Routes & CORS handler](docs_handler.md)
- [User & File model](docs_models.md)