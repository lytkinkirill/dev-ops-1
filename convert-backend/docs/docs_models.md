# User & File model

The user structure stores data about their username, password and email.

```go
type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}
```

The user file structure stores data about their username, filename and time of creation.

```go
type UserFile struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	Filename  string `json:"filename"`
	CreatedAt string `json:"created_at"`
}
```