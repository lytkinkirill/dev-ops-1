package handler

import (
	"AkBarsConverter/internal/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *Handler) login(c *gin.Context) {
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Проверка наличия пользователя в базе данных
	authorizedUser, err := h.r.UserRepo.GetUserByEmail(user.Email)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}

	username := authorizedUser.Username

	// Здесь также можно добавить проверку пароля, сравнив пароль пользователя и dbUser.Password

	c.JSON(http.StatusOK, gin.H{"message": "Авторизация прошла успешно", "success": "true", "username": username})
}

func (h *Handler) logout(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "Выход выполнен", "success": "true"})
}

func (h *Handler) register(c *gin.Context) {
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Проверяем, существует ли уже пользователь
	if _, err := h.r.UserRepo.GetUserByEmail(user.Email); err == nil {
		c.JSON(http.StatusConflict, gin.H{"message": "Такой пользователь уже существует!", "success": false})
		return
	}

	// Пользователь не существует, создаем нового пользователя
	if err := h.r.UserRepo.CreateUser(user.Username, user.Password, user.Email); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Возвращаем успешный ответ
	c.JSON(http.StatusCreated, gin.H{"message": "Пользователь успешно создан", "success": "true"})

}
