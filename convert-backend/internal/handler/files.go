package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

func (h *Handler) getFilesData(c *gin.Context) {
	// Распаковываем JSON-запрос
	var requestData map[string]interface{}
	if err := c.ShouldBindJSON(&requestData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Ошибка при разборе JSON"})
		return
	}

	// Получение имени пользователя из запроса
	_, ok := requestData["username"].(string)
	if !ok {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Некорректное имя пользователя"})
		return
	}
	var userFiles, err = h.r.FileRepo.GetFiles(requestData["username"].(string))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Ошибка при чтении списка файлов"})
		return
	}

	// Отправка данных на клиент в формате JSON
	c.JSON(http.StatusOK, userFiles)
}

func (h *Handler) uploadFile(c *gin.Context) {
	// Получаем файл из запроса
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(400, gin.H{"error": "Ошибка при получении файла"})
		return
	}
	defer file.Close()

	var filename string

	// Разделяем имя файла и расширение
	strings.TrimSpace(header.Filename)
	parts := strings.Split(header.Filename, ".")
	if len(parts) >= 2 {
		filenameWithoutExtension := strings.Join(parts[:len(parts)-1], ".")
		fileExtension := parts[len(parts)-1]

		// Получаем текущее время в формате Unix timestamp
		currentTime := time.Now().Unix()

		// Формируем новое имя файла с временной меткой перед расширением
		filename = fmt.Sprintf("%s_%d.%s", filenameWithoutExtension, currentTime, fileExtension)
	} else {
		fmt.Println("File name isn't processed")
	}

	// Определите путь для сохранения файла с временной меткой в имени
	//filename := fmt.Sprintf("%s_%d", header.Filename, time.Now().Unix())
	filepath := "../go/storage/" + filename

	// Создаем файл на сервере для сохранения
	uploadedFile, err := os.Create(filepath)
	if err != nil {
		fmt.Printf("Ошибка при создании файла: %s\n", err)
		c.JSON(500, gin.H{"error": "Ошибка при создании файла на сервере"})
		return
	}
	defer uploadedFile.Close()

	// Копируем содержимое файла из запроса в файл на сервере
	_, err = io.Copy(uploadedFile, file)
	if err != nil {
		c.JSON(500, gin.H{"error": "Ошибка при копировании файла"})
		return
	}

	c.JSON(200, gin.H{"message": "Файл успешно загружен и сохранен на сервере"})

	// Получаем имя пользователя
	username := c.PostForm("username")

	_ = time.Now()

	err = h.r.FileRepo.UploadFile(username, filename, time.Now())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Ошибка при записи пути в базу данных"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Файл успешно загружен и записан в базу данных", "success": "true"})
}

func (h *Handler) getFile(c *gin.Context) {
	// Распаковываем JSON-запрос
	/*var requestData map[string]interface{}
	if err := c.ShouldBindJSON(&requestData); err != nil {
		fmt.Printf("Ошибка при разборе JSON: %s\n", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": "Ошибка при разборе JSON"})
		return
	}*/
	fileId := c.Request.URL.Query().Get("id")

	// Получение filename  из запроса
	/*file, ok := requestData["filename"].(string)
	if !ok {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Некорректное имя пользователя"})
		return
	}*/

	// Ищем файл в базе данных
	filename, err := h.r.FileRepo.GetFile(fileId)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Файл не найден"})
		return
	}

	// Определяем путь к файлу в хранилище
	filePath := fmt.Sprintf("../go/storage/%s", filename)

	// Отправляем файл клиенту
	c.FileAttachment(filePath, filename)
}
