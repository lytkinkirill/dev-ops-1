package handler

import (
	"AkBarsConverter/internal/repository"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	r repository.Repositories
}

func NewHandler(r repository.Repositories) *Handler {
	return &Handler{
		r: r,
	}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()
	router.Use(CORSMiddleware())

	router.Static("/static", "../../static")

	api := router.Group("/api")
	{
		api.POST("/login", h.login)
		api.GET("/logout", h.logout)
		api.POST("/register", h.register)
		api.POST("/getFilesData", h.getFilesData)
		api.POST("/uploadFile", h.uploadFile)
		api.GET("/getFile", h.getFile)
	}

	return router
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
			return
		}
		c.Next()
	}
}
