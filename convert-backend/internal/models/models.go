package models

type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type UserFile struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	Filename  string `json:"filename"`
	CreatedAt string `json:"created_at"`
}
