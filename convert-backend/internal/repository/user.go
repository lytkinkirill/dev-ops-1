package repository

import (
	"AkBarsConverter/internal/models"
	"database/sql"
	"fmt"
)

type UserRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) UserRepositoryI {
	return UserRepository{db: db}
}

func (r UserRepository) GetUserByName(username string) (models.User, error) {
	var user models.User
	if err := r.db.QueryRow("SELECT * FROM users WHERE username = $1", username).
		Scan(&user.ID, &user.Username, &user.Password, &user.Email); err != nil {
		return user, fmt.Errorf("r.db.QueryRow.Scan: %w", err)
	}
	return user, nil
}

func (r UserRepository) GetUserByEmail(email string) (models.User, error) {
	var user models.User
	if err := r.db.QueryRow("SELECT * FROM users WHERE email = $1", email).
		Scan(&user.ID, &user.Username, &user.Password, &user.Email); err != nil {
		return user, fmt.Errorf("r.db.QueryRow.Scan: %w", err)
	}
	return user, nil
}

func (r UserRepository) CreateUser(username string, password string, email string) error {
	_, err := r.db.Exec("INSERT INTO users (username, password, email) VALUES ($1, $2, $3)", username, password, email)
	if err != nil {
		return fmt.Errorf("r.db.Exec: %w", err)
	}
	return nil
}
