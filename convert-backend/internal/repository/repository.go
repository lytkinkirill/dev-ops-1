package repository

import (
	"AkBarsConverter/internal/models"
	"database/sql"
	"time"
)

type Repositories struct {
	UserRepo UserRepositoryI
	FileRepo FileRepositoryI
}

func NewRepositories(db *sql.DB) Repositories {
	return Repositories{
		UserRepo: NewUserRepository(db),
		FileRepo: NewFileRepository(db),
	}
}

type UserRepositoryI interface {
	GetUserByName(string) (models.User, error)
	GetUserByEmail(string) (models.User, error)
	CreateUser(string, string, string) error
}

type FileRepositoryI interface {
	GetFiles(string) ([]models.UserFile, error)
	UploadFile(string, string, time.Time) error
	GetFile(string) (string, error)
}
