package repository

import (
	"AkBarsConverter/internal/models"
	"database/sql"
	"fmt"
	"time"
)

type FileRepository struct {
	db *sql.DB
}

func NewFileRepository(db *sql.DB) FileRepositoryI {
	return FileRepository{db: db}
}

func (r FileRepository) GetFiles(username string) ([]models.UserFile, error) {
	var file []models.UserFile
	rows, err := r.db.Query("SELECT id, filename, created_at FROM user_files WHERE username = $1", username)
	if err != nil {
		fmt.Printf("Ошибка при Select, %s\n", err.Error())
		return file, err
	}
	defer rows.Close()

	var files []models.UserFile
	for rows.Next() {
		var file models.UserFile
		err := rows.Scan(&file.ID, &file.Filename, &file.CreatedAt)
		if err != nil {
			fmt.Printf("Ошибка при сканировании: %s\n", err.Error())
			return nil, err
		}
		files = append(files, file)
	}

	return files, nil
}

func (r FileRepository) UploadFile(username, filename string, createdAt time.Time) error {
	query := "INSERT INTO user_files (username, filename, created_at) VALUES ($1, $2, $3)"
	_, err := r.db.Exec(query, username, filename, createdAt.Format("2006-01-02 15:04:05"))
	return err
}

func (r FileRepository) GetFile(fileId string) (string, error) {
	var foundFilename string
	query := "SELECT filename FROM user_files WHERE id = $1"
	err := r.db.QueryRow(query, fileId).Scan(&foundFilename)
	if err != nil {
		fmt.Printf("Ошибка при SELECT файла: %s\n", err.Error())
		return "", err
	}

	return foundFilename, nil
}
