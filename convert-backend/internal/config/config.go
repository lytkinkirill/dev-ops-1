package config

import (
	"AkBarsConverter/internal/provider/postgres"
	server "AkBarsConverter/internal/server/config"
	"fmt"
	"github.com/joho/godotenv"
	"os"
	"path/filepath"
)

const (
	ErrFieldNotFound = "field not found in environment variables"
)

type Config struct {
	Server   server.Config
	Postgres postgres.Config
}

func New() (Config, error) {
	var (
		cfg            Config
		serverConfig   server.Config
		postgresConfig postgres.Config
	)

	wd, err := os.Getwd()
	if err != nil {
		return cfg, fmt.Errorf("os.Getwd: %w", err)
	}

	envpath := filepath.Join(wd, ".env")

	err = godotenv.Load(envpath)
	if err != nil {
		return cfg, fmt.Errorf("godotenv.Load: %w", err)
	}

	// postgres configurations
	host := os.Getenv("DB_HOST")
	if err := Validate(host, "DB_HOST"); err != nil {
		return cfg, err
	}

	port := os.Getenv("DB_PORT")
	if err := Validate(port, "DB_PORT"); err != nil {
		return cfg, err
	}

	database := os.Getenv("DB_DATABASE")
	if err := Validate(database, "DB_DATABASE"); err != nil {
		return cfg, err
	}

	username := os.Getenv("DB_USERNAME")
	if err := Validate(username, "DB_USERNAME"); err != nil {
		return cfg, err
	}

	password := os.Getenv("DB_PASSWORD")
	if err := Validate(password, "DB_PASSWORD"); err != nil {
		return cfg, err
	}

	postgresConfig = postgres.Config{
		Host:     host,
		Port:     port,
		Database: database,
		Username: username,
		Password: password,
	}

	serverPort := os.Getenv("SERVER_PORT")
	if err := Validate(serverPort, "SERVER_PORT"); err != nil {
		return cfg, err
	}

	serverConfig = server.Config{
		Port: serverPort,
	}

	cfg = Config{
		Postgres: postgresConfig,
		Server:   serverConfig,
	}
	return cfg, nil
}

func Validate(val string, field string) error {
	if len(val) == 0 {
		return fmt.Errorf("%s %s", field, ErrFieldNotFound)
	}
	return nil
}
