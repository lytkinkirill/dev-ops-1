package postgres

type Config struct {
	Host     string
	Port     string
	Database string
	Username string
	Password string
}
