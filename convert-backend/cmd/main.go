package main

import (
	"AkBarsConverter/internal/config"
	"AkBarsConverter/internal/handler"
	"AkBarsConverter/internal/provider/postgres"
	"AkBarsConverter/internal/repository"
	"AkBarsConverter/internal/server"
	_ "github.com/lib/pq"
	"log"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	db, err := postgres.New(cfg.Postgres)
	if err != nil {
		log.Fatal(err)
	}

	r := repository.NewRepositories(db)

	handlers := handler.NewHandler(r)
	srv := new(server.Server)

	srv.Run(handlers.InitRoutes(), cfg.Server)
}
